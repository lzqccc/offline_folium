import pkg_resources
# 从名为 offline_folium 的包中获取一个名为 "local" 的资源文件的路径，并将其赋值给 dest_path 变量
dest_path = pkg_resources.resource_filename('offline_folium', "local")