* folium生成的html地图要加载在线的js，css，导致无法离线使用。因此，需要本地下载好需要的css和js，并修改生成的HTML文件。folium包含多个plugins，挨个下载起来比较麻烦。因此开发了一个自动下载css，js和自动修改html文件的包。
* 本项目参考了[offline_folium](https://github.com/robintw/offline_folium/)。由于原项目没有适配所有的插件，因此在该基础上对其升级。
* 需要的资源已上传至[offline folium](https://download.csdn.net/download/lzqccc/90096697)（https://download.csdn.net/download/lzqccc/90096697）
